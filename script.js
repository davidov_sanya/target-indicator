randomMinMax = (min, max) => {
  let rand = min + Math.random() * ( max + 1 - min );
  return Math.floor(rand);
};

init = () => {
  const target = randomMinMax(1, 99);
  const value = randomMinMax(0, target - 1);

  document.getElementById("targetValue").innerHTML = `${target}`;
  document.getElementById("infoValue").innerHTML = `${target - value}`;

  const thumb = document.getElementById("sliderThumb");
  const slider = document.getElementById("slider");
  const fill = document.getElementById("sliderFill");
  const sliderValue = document.getElementById("sliderValue");

  const pc = value / target;
  const loc = pc * slider.clientWidth;
  sliderValue.innerHTML = `${value}`;
  thumb.style.left =  loc - thumb.clientWidth/2 + "px";
  fill.style.width = loc + "px";

  if (slider.clientWidth < loc + thumb.clientWidth/2 - 4) {
    slider.style.marginRight = loc + thumb.clientWidth/2  - 4 - slider.clientWidth + "px";
  }

};